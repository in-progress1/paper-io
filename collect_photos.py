import os
from PIL import Image
from game_class import Game


datasets={
    'train':7,
    'test':3
}

def get_number_of_recorded_games(recording_dir):
    recorded_lives = os.listdir(recording_dir)
    number_of_recorded_lives = len(recorded_lives)
    return number_of_recorded_lives

def write_data(data, dir_name):
    #Creates dir
    image_dir = recording_dir+'/{0}'.format(dir_name)
    os.mkdir(image_dir)
    ###Writes images
    images = [item['state'] for item in data[0]]
    moves = [str(item['move']) for item in data[0]]
    for image_id in range(len(images)):
        image = images[image_id]
        Image.fromarray(image).save('{0}/{1}.png'.format(image_dir,image_id+1))
    with open(image_dir+'/moves.txt','w') as  moves_file:
        moves_string='\n'.join(moves)
        moves_file.write(moves_string)
    data.clear()


game=Game("HumanPlayer")


for dataset_type in datasets:
    recording_dir = './logs/images/'+dataset_type
    while get_number_of_recorded_games(recording_dir)<datasets[dataset_type]:
        try:
            game.play_game()
            if (game.score<=15):#Don't write bad games
                raise ValueError("It was a weak game")
        except:
            game.memory.data.clear()
            continue
        dir_name = get_number_of_recorded_games(recording_dir) + 1
        write_data(game.memory.data,dir_name)
        pass

del game

    
