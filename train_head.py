import tensorflow as tf
import numpy as np



from data import saved_model_file,ConfusionMatrix
from train_autoencoder import create_dataset, fix_bug

cnn_model_file = 'logs/cnn.h5'

L2=tf.keras.regularizers.l2(1e-5)
ReLu = tf.keras.activations.relu
Softmax = tf.keras.activations.softmax

def init_cnn():
    AE = tf.keras.models.load_model(saved_model_file)
    for ind in range(8):
        AE.get_layer(index = ind).trainable = False
    input_layer = AE.get_layer(index = 0).input
    features_layer = AE.get_layer(index = 7).output
    ds1 = tf.keras.layers.Dense(128,activation = ReLu, kernel_regularizer = L2, name='dense_1')(features_layer)
    ds2 = tf.keras.layers.Dense(32,activation = ReLu, kernel_regularizer = L2, name='dense_2')(ds1)
    ds3 = tf.keras.layers.Dense(3,activation = Softmax,name='predictions')(ds2)

    CNN = tf.keras.Model(inputs=input_layer, outputs=ds3)
    CNN.summary()
    return CNN



def compile_cnn(CNN):
    cp_callback = tf.keras.callbacks.ModelCheckpoint(cnn_model_file)#verbose=1
    tb_callback = tf.keras.callbacks.TensorBoard('logs/TensorBoard',profile_batch=0,write_graph=True)

    CNN.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
            loss=tf.keras.losses.CategoricalCrossentropy(), 
            metrics=[tf.keras.metrics.CategoricalAccuracy()])
    return cp_callback, tb_callback


if __name__ == '__main__':
    train_from_scratch = True#True

    class_factor = 15
    class_weight = {0: class_factor, 1: 1, 2:class_factor}
    fix_bug()

    
    train_dataset=create_dataset("train",type_of_dataset='train')
    test_dataset=create_dataset("test",type_of_dataset='train')

    if (train_from_scratch):
        CNN = init_cnn()
    else:
        CNN = tf.keras.models.load_model(cnn_model_file)
        CNN.summary()
    cp_callback, tb_callback = compile_cnn(CNN)
    confusion_matrix = ConfusionMatrix(CNN)
    



    CNN.fit(x = train_dataset,
        steps_per_epoch=500,
        epochs=10,
        callbacks=[cp_callback,tb_callback],
        validation_data=test_dataset,
        class_weight=class_weight
    )

    confusion_matrix(test_dataset)