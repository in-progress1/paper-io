import tensorflow as tf

#Mixed precision
#policy = tf.keras.mixed_precision.experimental.Policy('mixed_float16')
#tf.keras.mixed_precision.experimental.set_policy(policy)

from data import image_dim

L2=tf.keras.regularizers.l2(1e-5)
ReLu = tf.keras.activations.relu
dense_kwargs = {
    "kernel_regularizer":L2
}
conv_kwargs = {
    "padding":"same",
    "kernel_regularizer":L2
}
conv2d_transpose_kwarg = {
    "padding":"same",
    "kernel_regularizer":L2
}

def dense(x,units):
    
    dense = tf.keras.layers.Dense(units,activation = ReLu, **dense_kwargs)(x)
    return dense

def conv2d(x,filters,kernel_size=3,strides=1):

    conv = tf.keras.layers.Conv2D(filters=filters,kernel_size=kernel_size,strides=strides,activation=ReLu,**conv_kwargs)(x)
    return conv

def conv2d_transpose(x,filters,kernel_size=3,strides=1, end=False):
    activation = None if (end==True) else ReLu
    conv_transpose = tf.keras.layers.Conv2DTranspose(filters=filters, kernel_size= kernel_size, strides = strides, activation=activation,**conv2d_transpose_kwarg)(x)
    return conv_transpose

depth = 32

def create_nn():
    # Define encoder model.
    ##Add batchnorm remove bias
    encoder_input = tf.keras.Input(shape=image_dim, name='encoder_inputs')
    conv1 = conv2d(encoder_input,depth,3,1)
    conv2 = conv2d(conv1,depth*2,3,1)
    conv3 = conv2d(conv2,depth*2,3,1)
    conv4 = conv2d(conv3,depth*2,3,1)
    conv5 = conv2d(conv4,depth//4,3,1)
    fl1 = tf.keras.layers.Flatten()(conv5)
    ds1 = dense(fl1,512)
    encoded = ds1
    ######################
    # Define decoder model.
    decoder_input = encoded
    ds2 = dense(decoder_input, 32*32*8)
    rs1 = tf.keras.layers.Reshape([32,32,8])(ds2)
    conv1__transpose = conv2d_transpose(rs1,depth//4,3,1)
    conv2__transpose = conv2d_transpose(conv1__transpose,depth*2,3,1)
    conv3__transpose = conv2d_transpose(conv2__transpose,depth*2,3,1)
    conv4__transpose = conv2d_transpose(conv3__transpose,depth,3,1)
    conv5__transpose = conv2d_transpose(conv4__transpose,1,3,1, end=True)
    sigmoid1 = tf.keras.layers.Activation('sigmoid', dtype='float32', name='predictions')(conv5__transpose)
    decoded = sigmoid1
    # Define AE model.
    AE_output = decoded
    AE = tf.keras.Model(inputs=encoder_input, outputs=AE_output)
    return AE



if __name__ == '__main__':

    AE = create_nn()
    AE.summary()
    AE.save('logs/model.h5')