import tensorflow as tf
import numpy as np
import os 

from data import image_dim,model_file,saved_model_file

import matplotlib.pyplot as plt


batch_size=128


def fix_bug():
    ####
    gpu = tf.config.list_physical_devices('GPU')[0]
    tf.config.experimental.set_memory_growth(gpu, True)
    # tf.config.set_logical_device_configuration(
    #     gpu, [tf.config.LogicalDeviceConfiguration(memory_limit=6*1024)]) 
    ####

def show_output():
    AE = tf.keras.models.load_model(saved_model_file)
    plt.rcParams["figure.figsize"] = (8,8)
    for image_batch in test_dataset:
        input_images = image_batch[0].numpy()
        for input_image in input_images:
            input_image = np.expand_dims(input_image,0)
            output_image = AE.predict(input_image)
            f, axarr = plt.subplots(2)
            axarr[0].imshow(input_image.squeeze())#,cmap='gray'
            axarr[1].imshow(output_image.squeeze())#,cmap='gray'
            plt.show()

def map_function(x):
    string_array = tf.strings.split(x,sep=',')
    move_string, image_string = tf.split(string_array,[1,image_dim[0]*image_dim[1]])
    move = tf.strings.to_number(tf.squeeze(move_string),tf.dtypes.int64)
    one_hot_move = tf.one_hot(move,depth=3)
    image = tf.reshape(
            tf.strings.to_number(image_string),
        shape=image_dim)
    return image,one_hot_move


def create_dataset(ds_type,type_of_dataset='autoencoder'):
    files_dataset=tf.data.Dataset.list_files('./logs/files/{0}/*.data'.format(ds_type))
    dataset=tf.data.TextLineDataset(files_dataset,num_parallel_reads=8)
    if (ds_type=="train"):
        dataset=dataset.shuffle(1000, reshuffle_each_iteration=True)\
                        .repeat()
    dataset = dataset.map(
                        map_func = map_function,
                        num_parallel_calls=tf.data.experimental.AUTOTUNE)
    if (type_of_dataset=='autoencoder'):
        dataset = dataset.map(lambda x,y:(x,x),# Дублирует значение для inputs and targets
                            num_parallel_calls=tf.data.experimental.AUTOTUNE)
    elif (type_of_dataset=='train'):
        dataset = dataset

    dataset=dataset.batch(batch_size).prefetch(3)#,drop_remainder=True
    return dataset


def compile_nn(AE):
    cp_callback = tf.keras.callbacks.ModelCheckpoint(saved_model_file)#verbose=1
    tb_callback = tf.keras.callbacks.TensorBoard('logs/TensorBoard',profile_batch=0,write_graph=True)

    AE.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
            loss=tf.keras.losses.MeanSquaredError(), 
            metrics=[tf.keras.metrics.MeanSquaredError()])
    return cp_callback, tb_callback



if __name__ == '__main__':
    train_from_scratch = True
    fix_bug()



    train_dataset=create_dataset("train")
    test_dataset=create_dataset("test")
    load_model = model_file if (train_from_scratch) else saved_model_file

    AE = tf.keras.models.load_model(load_model)
    cp_callback, tb_callback = compile_nn(AE)
    AE.summary()



    AE.fit(x = train_dataset,
        steps_per_epoch=500,
        epochs=10,
        callbacks=[cp_callback,tb_callback],
        validation_data=test_dataset
    )

    show_output()

