import tensorflow as tf 
import numpy as np

from random import choice
from pynput import keyboard
from train_autoencoder import fix_bug
from data import image_dim

def create_player(playerType):
    if (playerType=="RandomPlayer"):
        return RandomPlayer()
    elif (playerType=="HumanPlayer"):
        return HumanPlayer()
    elif (playerType=="NNPlayer"):
        return NNPlayer()

class RandomPlayer:
    def __init__(self):
        self.moves=[0,1,2]

    def __del__(self):
        del self.moves

    def decide_move(self,game_state):
        return choice(self.moves)

class HumanPlayer:
    def __init__(self):
        def on_press(key):
            try:
                self.last_key = key.char
            except AttributeError:
                pass
        self.last_key = 'NoKey'

        self.listener = keyboard.Listener(
                                on_press=on_press)
        self.listener.start()

    def __del__(self):
        self.listener.stop()

    def get_key_press(self):
        key = self.last_key
        self.last_key = 'NoKey'
        return key

    ###f--h###
    def decide_move(self,game_state):
        key = self.get_key_press()
        if (key == 'f'): return 0
        elif (key == 'NoKey'): return 1
        elif (key == 'h'): return 2
        else: return 1
        
class NNPlayer:
    def __init__(self):#Trial version for estimating uptime
        fix_bug()
        self.CNN = tf.keras.models.load_model('logs/cnn.h5')
        self.image_size = image_dim[:2]
        self.image_preprocessing(np.full((900,900,3), 100))#image_dim
        
    def image_preprocessing(self, image):
        resized_image = tf.image.resize( image, size = self.image_size, method = tf.image.ResizeMethod.LANCZOS5)
        float_image = resized_image/255.0
        gs_image = tf.image.rgb_to_grayscale(float_image).numpy()

        ready_image = np.expand_dims(gs_image,axis=0)
        return ready_image

    def __del__(self):
        del self.CNN

    def decide_move(self,game_state):
        preprocessed_game_state = self.image_preprocessing(game_state)
        prediction = self.CNN.predict(preprocessed_game_state)
        move = prediction.argmax()
        return move