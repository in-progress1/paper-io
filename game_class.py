from time import sleep,time
#import atexit
import mss
import numpy as np


import selenium.common.exceptions
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from player_class import create_player
from data import Memory, Waiter

####GAME#####
class Game:

    def __init__(self,playerType):
        self.memory=Memory()
        self.init_driver()

        self.player=create_player(playerType)
        
        self.open_game()
        self.init_values()
        #atexit.register(lambda self:self.close_all(),self=self)

      
    def __del__(self):
        del self.player
        self.driver.close()
        self.driver.quit()

    def init_driver(self):
        self.sct = mss.mss()
        options = webdriver.ChromeOptions()
        options.add_argument(r'--load-extension=/home/bohdan/.config/google-chrome/Default/Extensions/cfhdojbkjhnklbpkdaibdccddilifddb/3.8_0')
        options.add_argument('--window-position=0,0')
        options.add_argument('--window-size=908,1031')#900x900 with open notification
        self.driver= webdriver.Chrome('./chromedriver', chrome_options=options)
        
        self.wait = WebDriverWait(self.driver, 10)

    def open_game(self):
        sleep(2)
        self.driver.switch_to_window(self.driver.window_handles[1])
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])
        #sleep(3)#Чтобы блокиратор рекламы успел включится
        self.driver.get('http://paper-io.com')
        sleep(2)

    def init_values(self):
        self.pre_game=self.driver.find_element(By.ID,'pre_game')
        self.the_game=self.driver.find_element(By.ID,'the_game')
        self.game_over=self.driver.find_element(By.ID,'game_over')

        self.game_screen  = self.driver.find_element_by_tag_name('body')

        self.all_keys={1:Keys.ARROW_LEFT,2:Keys.ARROW_UP,3:Keys.ARROW_RIGHT,4:Keys.ARROW_DOWN}

    def get_state(self):
        if (self.the_game.get_attribute('style')=='display: block;'):
            state='alive'#Если не alive, NN останавливается
        elif (self.game_over.get_attribute('style')=='display: block;'):
            state='died'
        elif (self.pre_game.get_attribute('style')=='display: block;'): 
            state='start'
        else:
            state='Unknown'
        return state

    def open_paper_io_mode(self):
        gamemode_dropdown=self.wait.until(EC.element_to_be_clickable((By.ID,'gamemode_dropdown')))
        gamemode_dropdown.click()
        paper_io_mode=self.wait.until(EC.element_to_be_clickable((By.ID,'section_paper1')))
        paper_io_mode.click()

    def click_start_button(self,css_selector):
        start_button=self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,css_selector)))
        start_button.click()
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME,'grid')))

    def start_game(self):
        state=self.get_state()
        if ((state=='start') or (state=='died')):
            if (state=='start'):
                css_selector='#pre_game > div.grow > div.button.play'
                self.open_paper_io_mode()
            elif (state=='died'):
                css_selector='#game_over > div.button.play'
            
            
            self.click_start_button(css_selector)

    def get_game_screen_position(self):          
        screen = self.game_screen.location
        window = self.driver.get_window_position()
        #Костыль, офсет задан как константные значения
        offset = {'x':4,'y':127}#~with open notification
        monitor = {"top": window['y']+screen['y']+offset['y'], 
                   "left": window['x']+screen['x']+offset['x']}
        monitor.update(self.game_screen.size)
        return monitor

    def get_screen(self):
        monitor = self.get_game_screen_position()
        img = self.sct.grab(monitor)
        numpy_bgra = np.array(img)
        numpy_image = np.flip(numpy_bgra[:, :, :3], 2)
        return numpy_image

    def make_move(self,move):
        turn=self.get_direction()
        if (turn=='item no longer exists'): 
            return 'already dead'
        if (move!=1):
            new_turn=turn+(move-1)
            if (new_turn<1): new_turn+=4
            if (new_turn>4): new_turn-=4
            self.game_screen.send_keys(self.all_keys[new_turn])

    def get_direction(self):##########################################???
        try: class_name=self.direction_element.get_attribute('class')
        except selenium.common.exceptions.StaleElementReferenceException:
            return 'item no longer exists'
        if ('turned_1' in class_name): return 1
        elif ('turned_2' in class_name): return 2
        elif ('turned_3' in class_name): return 3
        elif ('turned_4' in class_name): return 4

    def get_score(self):
        score=self.score_element.get_attribute('innerHTML')
        score=float(score[:-1])
        return score

    def get_takeover_reward(self):
        reward=0
        new_score=self.get_score()
        if (new_score>self.score):
            reward+=(new_score-self.score)*0.01#Сколько стоит еденица захваченого поля
        #Можно добавить защиту своего поля
        self.score=new_score
        return reward

    def init_game_params(self):
        self.score_element=self.driver.find_element(By.ID,'p1_status')
        self.score=self.get_score()
        self.direction_element=self.driver.find_element(By.ID,'p1_cursor')
        
    def del_game_params(self):
        del self.score_element
        del self.direction_element
        #del self.score


    def play_game(self):
        self.start_game()
        self.init_game_params()
        self.memory.add_life()

        waiter=Waiter(0.1)

        while (self.get_state()=='alive'):
            waiter.start_waiting()

            game_state=self.get_screen()
            move=self.player.decide_move(game_state)
        
            waiter.wait_the_remaining_time()
            last_reward=self.get_takeover_reward()#Награда приходит до совершения действия, ее нужно записывать в прошлый шаг
            if (self.make_move(move)=='already dead'): 
                continue
            self.memory.add_sample({'state':game_state,'move':move})
        self.del_game_params()
