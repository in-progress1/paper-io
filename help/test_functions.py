import tensorflow as tf
import numpy as np
from random import randint,shuffle

from time import time

def fix_bug():
    ####
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = True  # to log device placement (on which device the operation ran)
    sess = tf.compat.v1.Session(config=config)
    tf.compat.v1.keras.backend.set_session(sess)
    ####

fix_bug()

a = [i for i in range(0,5)]

def return_shuffle_repeat(a):
    b=[]
    for i in range(2):
        shuffle(a)
        b.extend(a)
    return b

b = return_shuffle_repeat(a)

print(b)















#################################
# t= time()
# a = [tf.data.Dataset.range(0,10) for i in range(100000)]
# print(time()-t,'secs')

#############################
#Masks

# samples, timesteps, features = 1, 10, 8
# inputs = np.random.random([samples, timesteps, features]).astype(np.float32)
# inputs[:, 3, :] = 0.
# inputs[:, 5, :] = 0.

# model = tf.keras.models.Sequential()
# model.add(tf.keras.layers.Masking(mask_value=0.,
#                                   input_shape=(timesteps, features)))
# model.add(tf.keras.layers.LSTM(1, return_sequences=True))
# model.summary()
# output = model(inputs)
# print(output.numpy())

################################

# # define CNN model
# inp = tf.keras.layers.Input((10,10,1))
# x = tf.keras.layers.Conv2D(1, (2,2), kernel_initializer='zeros', activation='relu', padding='same', )(inp)#input_shape=(10,10,1) ##Заменить на явный input layer
# x = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(x)
# x = tf.keras.layers.Flatten()(x)
# x = tf.keras.layers.Dense(10)(x)
# cnn = tf.keras.models.Model(inp,x)

# # define LSTM model
# inp = tf.keras.layers.Input((None,10,10,1))
# x = tf.keras.layers.TimeDistributed(cnn)(inp)#,input_shape = (None,10,10,1)
# x = tf.keras.layers.LSTM(5, return_sequences=True)(x)
# x = tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(1))(x)
# model = tf.keras.models.Model(inp,x)

# cnn.summary()
# model.summary()

# input_data=np.ones(shape=[2,3,10,10,1],dtype=np.float32)
# prediction = model.predict(input_data)
# print(prediction)


###########################


# a=np.ones(shape=[128,128,1])

# data = [[a]*randint(5,10) for i in range(10)]

# dataset = tf.data.Dataset.from_generator(lambda: iter(data), tf.int32) 
# dataset = dataset.padded_batch(2, padded_shapes=[None,*a.shape]) 

# for element in dataset.as_numpy_iterator():
#     print(element)
#     print('end') 