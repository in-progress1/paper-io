import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.mixed_precision import experimental as mixed_precision

def fix_bug():
    ####
    gpu = tf.config.list_physical_devices('GPU')[0]
    tf.config.set_logical_device_configuration(
        gpu, 
        [tf.config.LogicalDeviceConfiguration(memory_limit=6*1024)]) 
    #tf.config.experimental.set_memory_growth(gpu, True)
    ####

#fix_bug()

policy = mixed_precision.Policy('mixed_float16')#mixed_float16
mixed_precision.set_policy(policy)

print('Compute dtype: %s' % policy.compute_dtype)
print('Variable dtype: %s' % policy.variable_dtype)

num_units = 4096*2

inputs = keras.Input(shape=(784,), name='digits')
x = layers.Reshape(target_shape=[28,28,1])(inputs)
x = layers.Conv2D(filters=1024,kernel_size=3,padding='same',activation='relu')(x)
x = layers.MaxPool2D(strides=2)(x)
x = layers.Conv2D(filters=1024,kernel_size=3,padding='same',activation='relu')(x)
x = layers.MaxPool2D(strides=2)(x)
x = layers.Flatten()(x)
# x = layers.Dense(num_units, activation='relu')(x)
# x = layers.Dense(num_units, activation='relu')(x)
# x = layers.Dense(num_units, activation='relu')(x)

# CORRECT: softmax and model output are float32
x = layers.Dense(128)(x)
x = layers.Dense(10, name='dense_logits')(x)
outputs = layers.Activation('softmax', dtype='float32', name='predictions')(x)
print('Outputs dtype: %s' % outputs.dtype.name)

model = keras.Model(inputs=inputs, outputs=outputs)
model.compile(loss='sparse_categorical_crossentropy',
              optimizer=keras.optimizers.RMSprop(),
              metrics=['accuracy'])

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
x_train = x_train.reshape(60000, 784).astype('float32') / 255
x_test = x_test.reshape(10000, 784).astype('float32') / 255

initial_weights = model.get_weights()

history = model.fit(x_train, y_train,
                    batch_size=32,
                    epochs=100,
                    validation_split=0.95,
                    validation_freq=100)
test_scores = model.evaluate(x_test, y_test, verbose=2)
print('Test loss:', test_scores[0])
print('Test accuracy:', test_scores[1])