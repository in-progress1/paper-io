def prepare_training_data(self,data,batch_size):
        #special_data=list(filter(lambda x:x['reward']!=0,data))
        #data+=special_data*10
        data=data[:len(data)-len(data)%batch_size]
        rewards=[sample['reward'] for sample in data]
        next_states=[sample['next_state'] for sample in data]
        
        q_next_value=self.model.predict(np.array(next_states))#,dtype=np.string
        q_action=q_next_value.argmax(axis=1)
        q_action_value=np.amax(q_next_value,axis=1)

        ends=[sample['end'] for sample in data]
        q_action_value=[q_v if (end==False) else 0 for q_v,end in zip(q_action_value,ends)]#Нет будущего, зануляем
        q_targets=[x[0]+0.9*x[1] for x in zip(rewards, q_action_value)]
        actions=[sample['move'] for sample in data]
        states=[sample['state'] for sample in data]#Если что добавить float()
        #Если не соединятся, доабвить np.stack

        return np.array(states), np.dstack([q_targets, actions])[0]

def train_nn(self):#Не сделано
        batch_size=5
        states, q_targets_and_actions=self.prepare_training_data(self.memory.data,batch_size)
        self.model.fit(x=states,y=q_targets_and_actions,
                                batch_size=batch_size,epochs=5,callbacks=[self.cp_callback])#По оконнию сохранять
        #self.saver.save(self.sess, 'recovery data\\graph',latest_filename='variables.ckpt')



#move=self.model.predict(x=np.array([game_state])).argmax(axis=1)[0]

def init_nn(self):
        self.sess=tf.Session()
        tf.keras.backend.set_session(self.sess)
        self.model=tf.keras.models.load_model('logs/model_trained.h5')
        self.model.predict([[self.driver.find_element_by_tag_name('body').screenshot_as_png]])

        self.cp_callback=tf.keras.callbacks.ModelCheckpoint('logs/model_trained.h5',
                                                 verbose=1,period=5)