import pickle
from time import sleep,time

import numpy as np
from matplotlib import pyplot as plt
import sklearn.metrics

image_dim=[32,32,1]

model_file='logs/model.h5'
saved_model_file = 'logs/model_trained.h5'



class Memory:
    def __init__(self):
        self.data=self.restore()

    def add_life(self):
        self.data.append([])

    def add_sample(self,sample):
        self.data[-1].append(sample)

    def save(self):
        with open('logs/data.pickle','wb') as file:
            pickle.dump(self.data,file)
    
    def restore(self):
        try: 
            with open('logs/data.pickle','rb') as file:
                data=pickle.load(file)
        except FileNotFoundError:
            data=[]

        return data


####Waiter####
class Waiter:

    def __init__(self,delay):
        self.delay=delay

    def start_waiting(self):
        self.time=time()

    def wait_the_remaining_time(self):
        remaining_time=self.delay-(time()-self.time)
        print(remaining_time)
        if (remaining_time>0):
            sleep(remaining_time)


####ConfusionMatrix####
class ConfusionMatrix:

    def __init__(self,CNN):
        self.CNN = CNN

    def __call__(self,dataset):
        self.show_confusion_matrix(dataset)

    def plot_confusion_matrix(self,cm, class_names):
        """
        Returns a matplotlib figure containing the plotted confusion matrix.    
        Args:
          cm (array, shape = [n, n]): a confusion matrix of integer classes
          class_names (array, shape = [n]): String names of the integer classes
        """
        plt.figure(figsize=(9, 9))
        plt.imshow(cm, interpolation='nearest', cmap='Blues')
        plt.title("Confusion matrix")
        #plt.colorbar()
        tick_marks = np.arange(len(class_names))
        plt.xticks(tick_marks, class_names, rotation=45)
        plt.yticks(tick_marks, class_names) 
        # Use white text if squares are dark; otherwise black.
        threshold = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                plt.text(j, i, cm[i, j], horizontalalignment="center", 
                         color="white" if cm[i, j] > threshold else "black")   
        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.show()


    def get_confusion_matrix(self,val_labels, val_pred):
        cm = sklearn.metrics.confusion_matrix(val_labels, val_pred)#val_labels хранятся в dataset
        # Normalize the confusion matrix.
        #cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)
        return cm

    def show_confusion_matrix(self,dataset):
        val_pred_raw = self.CNN.predict(x=dataset)
        val_pred = np.argmax(val_pred_raw, axis=1)

        val_labels = None
        for test_batch in dataset:
            test_batch = test_batch[1].numpy()
            if (val_labels is None): 
                val_labels = test_batch
            else:
                val_labels = np.concatenate((val_labels,test_batch))
        val_labels = np.argmax(val_labels, axis=1)

        cm=self.get_confusion_matrix(val_labels, val_pred)
        self.plot_confusion_matrix(cm, class_names=[0,1,2])  