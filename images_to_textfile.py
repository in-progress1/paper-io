import tensorflow as tf
import numpy as np
import os

from data import image_dim

dir_types=['test','train']

batch_size=100

def create_dataset(ds_type,game_id):
    dataset=tf.data.Dataset.list_files("./logs/images/{0}/{1}/*.png".format(ds_type,game_id),shuffle=False)
    dataset=dataset.map(lambda x: tf.image.resize(
                            tf.image.convert_image_dtype(
                                tf.io.decode_png(
                                    tf.io.read_file(x),
                                channels=1),
                            dtype=tf.float32),
                        size=image_dim[0:2],method=tf.image.ResizeMethod.LANCZOS5),
                    num_parallel_calls=tf.data.experimental.AUTOTUNE)
    moves_dataset = tf.data.TextLineDataset("./logs/images/{0}/{1}/moves.txt".format(ds_type,game_id))
    dataset = tf.data.Dataset.zip((moves_dataset,dataset))
    dataset=dataset.batch(batch_size)
    return dataset




for dir_type in dir_types:
    recording_dir='./logs/files/'+dir_type
    image_dirs = os.listdir('./logs/images/'+dir_type)
    for ind in range(len(image_dirs)):
        image_dir = image_dirs[ind]
        dataset=create_dataset(dir_type,image_dir)
        with open(recording_dir+'/{0}.data'.format(ind+1),'w')as file:
            for data_batch in dataset:
                moves_batch = data_batch[0].numpy().astype('str')
                image_batch = data_batch[1].numpy().astype('str')
                array_strings = [move+','+','.join(numpy_image.flatten()) for move,numpy_image in zip(moves_batch,image_batch)]
                array_string = '\n'.join(array_strings)+'\n'
                file.write(array_string)